import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { authGuard } from './core/guard/auth.guard';
import { adminGuard } from './core/guard/auth-admin.guard';
import { AppLayoutComponent } from './layout/app.layout.component';

const routes: Routes = [
    {
        path: '', component: AppLayoutComponent,
        children: [
            {
                path: '',
                data: { breadcrumb: 'Main' },
                canActivate: [authGuard],
                loadChildren: () => import('./components/apps/main/main.module').then(m => m.MainModule)
            },
        ]
    },
    // { path: 'auth', data: { breadcrumb: 'Auth' }, loadChildren: () => import('./demo/components/auth/auth.module').then(m => m.AuthModule) },
    { path: 'login', data: { breadcrumb: 'Login' }, loadChildren: () => import('./components/auth/login/login.module').then(m => m.LoginModule) },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
