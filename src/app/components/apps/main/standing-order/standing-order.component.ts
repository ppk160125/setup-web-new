import { Component, OnInit } from '@angular/core';
import { Table } from 'primeng/table';
import { StandingOrderService } from './standing-order-service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-setup-standing-order',
    templateUrl: './standing-order.component.html',
    styleUrls: ['./standing-order.component.scss'],
})
export class StandingOrderComponent {
    blockedPanel: boolean = false;

    data: any[] = [];
    type: any[] = [];
    ward: any[] = [];
    standingorder: any;
    selectecId: any;
    selectedData: any;
    
    isNew: boolean = false;
    displayDialog: boolean = false;

    data_id: any;
    data_name: any;
    data_description: any;
    data_status: boolean = true;

    orderType: any = [
        { name: 'orderType1', code: '1' },
        { name: 'orderType2', code: '2' },
    ];

    users: any= [] = [];

    messages : any = '';

    title = '   Standing-Order';

    constructor(
        private standingOrderService: StandingOrderService,
        private router: Router
    ) {}

    async ngOnInit() {
        await this.getBedType();
        await this.getData();
        await this.getStandingOrder();
        await this.getItems();
        await this.getGroupDisease();
        await this.getMeditemUsage();
        await this.getDepartment();
    }

    async getStandingOrder() {
        try{
            const res: any = await this.standingOrderService.getStandingOrder();
        if(res.data.data){
            this.type=res.data.data;
        }
            console.log(res)
        }catch(error){
            console.log(error)
        }
    }

    async getGroupDisease(){
        try{
            const res: any = await this.standingOrderService.get();
        if(res.data.data){
            this.data=res.data.data;
            for(let i of this.data){
                i.bed_type = this.type.find((x:any)=> i.bed_type_id == x.id).name
            }
        }
            console.log(res)
        }catch(error){
            console.log(error)
        }
      }

      async getMeditemUsage(){
        try{
            const res: any = await this.standingOrderService.get();
        if(res.data.data){
            this.data=res.data.data;
            for(let i of this.data){
                i.bed_type = this.type.find((x:any)=> i.bed_type_id == x.id).name
            }
        }
            console.log(res)
        }catch(error){
            console.log(error)
        }
      }

      async getDepartment(){
        try{
            const res: any = await this.standingOrderService.get();
        if(res.data.data){
            this.data=res.data.data;
            for(let i of this.data){
                i.bed_type = this.type.find((x:any)=> i.bed_type_id == x.id).name
            }
        }
            console.log(res)
        }catch(error){
            console.log(error)
        }
      }

    async getItems(){
        try{
            const res: any = await this.standingOrderService.get();
        if(res.data.data){
            this.data=res.data.data;
            for(let i of this.data){
                i.bed_type = this.type.find((x:any)=> i.bed_type_id == x.id).name
            }
        }
            console.log(res)
        }catch(error){
            console.log(error)
        }
      }

    async getData() {
        try{
            const res: any = await this.standingOrderService.get();
        if(res.data.data){
            this.data=res.data.data;
            for(let i of this.data){
                i.bed_type = this.type.find((x:any)=> i.bed_type_id == x.id).name
            }


        }
            console.log(res)
        }catch(error){
            console.log(error)
        }
    }

    async getBedType() {
        try{
            const res: any = await this.standingOrderService.getBedType();
        if(res.data.data){
            this.type=res.data.data;
        }
            console.log(res)
        }catch(error){
            console.log(error)
        }
    }

    async getWard() {
        try{
            const res: any = await this.standingOrderService.getWard();
        if(res.data.data){
            this.type=res.data.data;
        }
            console.log(res)
        }catch(error){
            console.log(error)
        }
    }

    async saveData() {
        let info={
            description: this.data_name,
            is_active: this.data_status
        }

        try{
            let res: any=await this.standingOrderService.save(info)
        }catch(error){
            console.log(error)
        }
        this.clearData();
        this.displayDialog = false;
        this.getData();
    }

    async updateData() {
        let info={
            description: this.data_name,
            is_active: this.data_status
        }

        try{
            let res: any=await this.standingOrderService.update(this.data_id,info)
        }catch(error){
            console.log(error)
        }
        this.clearData();
        this.displayDialog = false;
        this.getData();
    }

    async deleteData(data: any) {
        console.log(data);
        let id=data.id;
        try{
            let res: any=await this.standingOrderService.delete(id)
        }catch(error){
            console.log(error)
        }
        this.clearData();
        this.displayDialog = false;
        this.getData();
    }

    async showEditDialog(data: any) {
        this.isNew = false;
        this.displayDialog = true;
        this.data_name=data.description;
        this.data_id=data.id;
        this.data_status=data.is_active;

    }

    async showAddDialog() {
        this.isNew = true;
        this.displayDialog = true;

    }

    async showDeleteDialog() {
    }

    clearData() {
        this.data_id = '';
        this.data_name = '';
        this.data_description = '';
        this.data_status = true;
    }

    back() {
        this.router.navigate(['/']);
    }

    onGlobalFilter(table: Table, event: Event) {
        table.filterGlobal(
            (event.target as HTMLInputElement).value,
            'contains'
        );
    }

}