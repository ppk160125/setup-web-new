import { Injectable } from '@angular/core';
import axios from 'axios';
import { environment } from '../../../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class ImportService {

    pathPrefixImport: any = `api-his/import-paperless`
    pathPrefixDoctor: any = `api-doctor/doctor`
    pathPrefixAuth: any = `api-auth/auth`

    private axiosInstance = axios.create({
        baseURL: `${environment.apiUrl}${this.pathPrefixImport}`
    });

    private axiosInstancedtr = axios.create({
        baseURL: `${environment.apiUrl}${this.pathPrefixDoctor}`
    });

    constructor() {
        this.axiosInstance.interceptors.request.use(config => {
            const token = sessionStorage.getItem('token');
            if (token) {
                config.headers['Authorization'] = `Bearer ${token}`;
            }
            return config;
        });

        this.axiosInstancedtr.interceptors.request.use(config => {
            const token = sessionStorage.getItem('token');
            if (token) {
                config.headers['Authorization'] = `Bearer ${token}`;
            }
            return config;
        });

        this.axiosInstance.interceptors.response.use(response => {
            return response;
        }, error => {
            return Promise.reject(error);
        });

        this.axiosInstancedtr.interceptors.response.use(response => {
            return response;
        }, error => {
            return Promise.reject(error);
        })
    }

    async getWard() {
        const url = `/ward`;
        return this.axiosInstance.get(url);
    }

    async getDepartment() {
        const url = `/department`;
        return this.axiosInstance.get(url);
    }

    async getFood() {
        const url = `/food`;
        return this.axiosInstance.get(url);
    }

    async getInsurance() {
        const url = `/insurance`;
        return this.axiosInstance.get(url);
    }

    async getReferBy() {
        const url = `/refer-by`;
        return this.axiosInstance.get(url);
    }

    async getReferCause() {
        const url = `/refer-cause`;
        return this.axiosInstance.get(url);
    }

    async getMedicine() {
        const url = `/medicine`;
        return this.axiosInstance.get(url);
    }

    async getMedicineUsage() {
        const url = `/medicine-usage`;
        return this.axiosInstance.get(url);
    }

    async getItems() {
        const url = `/items`;
        return this.axiosInstance.get(url);
    }

    async getSpecialist() {
        const url = `/specialist`;
        return this.axiosInstance.get(url);
    }

}