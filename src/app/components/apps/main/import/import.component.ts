import { Component, OnInit } from '@angular/core';
import { Table } from 'primeng/table';
import { ImportService } from './import.service';

@Component({
    selector: 'app-import',
    templateUrl: './import.component.html',
    styleUrls: ['./import.component.scss'],
})

export class ImportComponent {

    importData : any = [];

    constructor(
        private importService: ImportService
    ) {
    }

    ngOnInit(): void {
    }

    async getWard() {
        let rs:any = await this.importService.getWard();
        if(rs.status = 'ok'){
            this.importData = rs.data;
        }
    }

    async getImportData() {
        console.log('getImportData');
        // import department
        // import ward 
        
    }
}