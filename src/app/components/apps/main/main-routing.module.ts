import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent} from './main.component';
import { UserComponent } from './user/user.component';
import { ImportComponent } from './import/import.component';
import { ActivityComponent } from './activity/activity.component';
import { EvaluateComponent } from './evaluate/evaluate.component';
import { StandingOrderComponent } from './standing-order/standing-order.component';
import { BedComponent } from './bed/bed.component';

const routes: Routes = [
    { path: '', component: MainComponent },
    { path: 'user', component: UserComponent },
    { path: 'import', component: ImportComponent },
    { path: 'activity', component: ActivityComponent },
    { path: 'evaluate', component: EvaluateComponent },
    { path: 'standing-order', component: StandingOrderComponent },
    { path: 'bed', component: BedComponent }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }


