import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonModule } from 'primeng/button';
import { FormsModule } from '@angular/forms';
import { RadioButtonModule } from 'primeng/radiobutton';
import { StyleClassModule } from 'primeng/styleclass';
import { RippleModule } from 'primeng/ripple';
import { AppConfigModule } from 'src/app/layout/config/config.module';
import { DialogModule } from 'primeng/dialog';
import { InputTextModule } from "primeng/inputtext";
import { InputSwitchModule } from 'primeng/inputswitch';
import { TableModule } from 'primeng/table';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { MessagesModule } from 'primeng/messages';
import { ToastModule } from 'primeng/toast';
import { ProgressBarModule } from 'primeng/progressbar';
import { TreeSelectModule } from 'primeng/treeselect';
import { AutoCompleteModule } from "primeng/autocomplete";
import { BlockUIModule } from 'primeng/blockui';
import { TagModule } from 'primeng/tag';
import { FileUploadModule } from 'primeng/fileupload';
import { InputNumberModule } from 'primeng/inputnumber';
import { TooltipModule } from 'primeng/tooltip';
import { AvatarModule } from 'primeng/avatar';
import { DropdownModule } from 'primeng/dropdown';

import { DividerModule } from 'primeng/divider';
import { NgxSpinnerModule } from "ngx-spinner";
import { MainRoutingModule } from './main-routing.module';
import { UserComponent } from './user/user.component';
import { MainComponent } from './main.component';
import { ImportComponent } from './import/import.component';
import { ActivityComponent } from './activity/activity.component';
import { EvaluateComponent } from './evaluate/evaluate.component';
import { StandingOrderComponent } from './standing-order/standing-order.component';
import { BedComponent } from './bed/bed.component';

@NgModule({
  declarations: [
    MainComponent,
    UserComponent,
    ImportComponent,
    ActivityComponent,
    EvaluateComponent,
    StandingOrderComponent,
    BedComponent
  ],
  imports: [
    CommonModule,
    MainRoutingModule,
    ButtonModule,
    FormsModule,
    RadioButtonModule,
    StyleClassModule,
    RippleModule,
    AppConfigModule,
    DialogModule,
    InputTextModule,
    InputSwitchModule,
    TableModule,
    ProgressSpinnerModule,
    MessagesModule,
    ToastModule,
    ProgressBarModule,
    TreeSelectModule,
    AutoCompleteModule,
    BlockUIModule,
    TagModule,
    FileUploadModule,
    InputNumberModule,
    TooltipModule,
    AvatarModule,
    ButtonModule,
    DividerModule,
    DropdownModule,
    NgxSpinnerModule
  ]
})
export class MainModule { }