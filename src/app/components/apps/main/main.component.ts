import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { NgxSpinnerService } from 'ngx-spinner';
import { SelectItem } from 'primeng/api';


@Component({
    selector: 'app-main',
    templateUrl: './main.component.html',
    styleUrls: ['./main.component.scss'],
})
export class MainComponent implements OnInit {
    query: any = '';
    dataSet: any[] = [];
    loading = false;
    total = 0;
    pageSize = 20;
    pageIndex = 1;
    offset = 0;
    user_login_name: any;
    item: any = [1, 2, 3, 4, 5, 6, 7, 8, 9];
    wards: any = [];

    sortOptions: SelectItem[] = [];

    sortOrder: number = 0;

    sortField: string = '';

    sourceCities: any[] = [];

    targetCities: any[] = [];

    orderCities: any[] = [];
    filterName:string = '';

    constructor(
        private router: Router,
        private spinner: NgxSpinnerService
    ) {
        this.user_login_name = sessionStorage.getItem('userLoginName');
    }

    ngOnInit(): void {
        this.sortOptions = [
            { label: 'ก --> ฮ', value: 'name' },
            { label: 'ฮ --> ก', value: '!name' },
        ];
    }
    /////////////เมธอด หลัก///////////ที่ต้องมี///////////////
    hideSpinner() {
        setTimeout(() => {
            this.spinner.hide();
        }, 1000);
    }

    goToUser() {
        this.router.navigate(['/user']);
    }

    gotoImport() {
        this.router.navigate(['/import']);
    }

    gotoActivity() {
        this.router.navigate(['/activity']);
    }

    gotoEvaluate() {
        this.router.navigate(['/evaluate']);
    }

    gotoStandingOrder() {
        this.router.navigate(['/standing-order']);
    }

    gotoBed() {
        this.router.navigate(['/bed']);
    }
}
