import { Injectable } from '@angular/core';
import axios from 'axios';
import { environment } from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ActivityService {

  pathPrefixLookup: any = `api-lookup/lookup/activity`
  pathPrefixDoctor: any = `api-doctor/doctor`
  pathPrefixAuth: any = `api-auth/auth`

  private axiosInstance = axios.create({
    baseURL: `${environment.apiUrl}${this.pathPrefixLookup}`
  });

  private axiosInstancedtr = axios.create({
    baseURL: `${environment.apiUrl}${this.pathPrefixDoctor}`
  });

  constructor () {
    this.axiosInstance.interceptors.request.use(config => {
      const token = sessionStorage.getItem('token');
      if (token) {
        config.headers['Authorization'] = `Bearer ${token}`;
      }
      return config;
    });

    this.axiosInstancedtr.interceptors.request.use(config => {
      const token = sessionStorage.getItem('token');
      if (token) {
        config.headers['Authorization'] = `Bearer ${token}`;
      }
      return config;
    });

    this.axiosInstance.interceptors.response.use(response => {
      return response;
    }, error => {
      return Promise.reject(error);
    });

    this.axiosInstancedtr.interceptors.response.use(response => {
      return response;
    }, error => {
      return Promise.reject(error);
    })
  }

  async getActivity() {
    return await this.axiosInstance.get('/list');
  }
  async deleteActivity(id: any) {
    return await this.axiosInstance.delete('/' + id);
  }
  async updateActivity(id: any, data: any) {
    return await this.axiosInstance.put('/' + id, data);
  }
  async saveActivity(data: any) {
    return await this.axiosInstance.post('', data);
  }
  async getActivityByID(id: any) {
    return await this.axiosInstance.post('/' + id);
  }
}
